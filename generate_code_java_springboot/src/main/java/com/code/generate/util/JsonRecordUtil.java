package com.code.generate.util;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Record;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class JsonRecordUtil {
    /**
     * jfinal的List<Record> list需要处理一下
     * 转成List<Map>方便使用
     * @param list
     * @return
     */
    public static List convertRecords(List<Record> list){
        if(CollectionUtil.isEmpty(list)){
            return Collections.emptyList();
        }
        String data = JsonKit.toJson(list);
        JSONArray dataList = JSONUtil.parseArray(data);
        return JSON.parseArray(data,Map.class);
    }
    public static List convertList(List list){
        if(CollectionUtil.isEmpty(list)){
            return Collections.emptyList();
        }
        String data = JsonKit.toJson(list);
        JSONArray dataList = JSONUtil.parseArray(data);
        return JSON.parseArray(data,Map.class);
    }
}
