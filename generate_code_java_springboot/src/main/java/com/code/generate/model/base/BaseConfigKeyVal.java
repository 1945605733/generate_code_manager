package com.code.generate.model.base;

import com.jfinal.plugin.activerecord.IBean;
import com.jfinal.plugin.activerecord.Model;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseConfigKeyVal<M extends BaseConfigKeyVal<M>> extends Model<M> implements IBean {

    public M setId(Integer id) {
        set("id", id);
        return (M) this;
    }

    public Integer getId() {
        return getInt("id");
    }

    public M setConfigId(Integer configId) {
        set("config_id", configId);
        return (M) this;
    }

    public Integer getConfigId() {
        return getInt("config_id");
    }

    public M setName(String name) {
        set("name", name);
        return (M) this;
    }

    public String getName() {
        return getStr("name");
    }

    public M setVal(String val) {
        set("val", val);
        return (M) this;
    }

    public String getVal() {
        return getStr("val");
    }

    public M setVal1(String val1) {
        set("val1", val1);
        return (M) this;
    }

    public String getVal1() {
        return getStr("val1");
    }

    public M setVal2(String val2) {
        set("val2", val2);
        return (M) this;
    }

    public String getVal2() {
        return getStr("val2");
    }

    public M setCheck(Integer check) {
        set("check", check);
        return (M) this;
    }

    public Integer getCheck() {
        return getInt("check");
    }

    public M setListImportPkg(String listImportPkg) {
        set("listImportPkg", listImportPkg);
        return (M) this;
    }

    public String getListImportPkg() {
        return getStr("listImportPkg");
    }

}
