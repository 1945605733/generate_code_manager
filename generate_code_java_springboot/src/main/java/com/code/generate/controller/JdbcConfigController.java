package com.code.generate.controller;


import com.code.generate.model.JdbcConfig;
import com.code.generate.vo.ResultData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/jdbcConfig")
public class JdbcConfigController {

    @RequestMapping(value = "/list", method = {RequestMethod.POST,RequestMethod.GET})
    public ResultData list() {
        List<JdbcConfig> list = JdbcConfig.dao.find("select * from jdbc_config");
        log.info("list:{}",list);
        return ResultData.buildSuccess(list);
    }
    @RequestMapping(value = "/list1", method = {RequestMethod.POST,RequestMethod.GET})
    public ResultData list1() {
       Map<String,Integer> map = new HashMap<>();
        map.put("jdbc_config_id",1);
        return ResultData.buildSuccess(map);
    }
    @RequestMapping(value = "/del",method = {RequestMethod.POST,RequestMethod.GET})
    public ResultData del(@RequestParam("id")String id) {
        JdbcConfig.dao.deleteById(id);
        return ResultData.buildSuccess();
    }

    @RequestMapping(value = "/get", method = {RequestMethod.POST,RequestMethod.GET})
    public ResultData get(@RequestParam("id")String id) {
        return ResultData.buildSuccess(JdbcConfig.dao.findById(id));
    }
    @RequestMapping(value = "/saveUpdate")
    public ResultData saveUpdate(@RequestParam(value = "name",required = false)String name,
                           @RequestParam(value = "dbName",required = false)String dbName,
                           @RequestParam(value = "user",required = false)String user,
                           @RequestParam(value = "password",required = false)String password,
                           @RequestParam(value = "jdbcUrl",required = false)String jdbcUrl,
                           @RequestParam(value = "jdbcConfigId",required = false)Integer jdbcConfigId) {
        JdbcConfig jdbcConfig = new JdbcConfig();
        jdbcConfig.setName(name);
        jdbcConfig.setDbName(dbName);
        jdbcConfig.setUser(user);
        jdbcConfig.setPassword(password);
        jdbcConfig.setJdbcUrl(jdbcUrl);
        if (jdbcConfigId == null) {
            jdbcConfig.save();
        } else {
            jdbcConfig.setJdbcConfigId(jdbcConfigId);
            jdbcConfig.update();
        }
        return ResultData.buildSuccess();
    }
}