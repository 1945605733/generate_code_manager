package com.code.generate.controller;


import com.code.generate.model.Config;
import com.code.generate.model.ConfigKeyVal;
import com.code.generate.vo.ResultData;
import com.jfinal.plugin.activerecord.Db;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@Slf4j
@RestController
@RequestMapping("/config")
public class ConfigController{

    @RequestMapping(value = "/list", method = {RequestMethod.POST,RequestMethod.GET})
    public ResultData list(@RequestParam("projectId") String projectId) {
        List<Config> listConfig = Config.dao.find("select * from config where project_id=?", projectId);
        if (listConfig != null && listConfig.size() > 0) {
            for (int i = 0; i < listConfig.size(); i++) {
                List<ConfigKeyVal> listConfigKeyVal = ConfigKeyVal.dao
                        .find("select * from configKeyVal where config_id=?", listConfig.get(i).getConfigId());
                if (listConfigKeyVal != null) {
                    listConfig.get(i).put("keyValList", listConfigKeyVal);
                } else {
                    listConfig.get(i).put("keyValList", new ArrayList<Object>());
                }

            }
        }
        return ResultData.buildSuccess(listConfig);
    }
    @RequestMapping(value = "/delConfig", method = {RequestMethod.POST,RequestMethod.GET})
    public ResultData delConfig(@RequestParam("id")String id) {
        Config config = Config.dao.findById(id);
        if (config == null) {
            return ResultData.buildFailure("查无数据，删除失败");
        }
        Db.delete("delete from config_record where project_id=? and config_id=?", config.getProjectId(), config.getConfigId());
        config.delete();
        return ResultData.buildSuccess();
    }
    @RequestMapping(value = "/delConfigKeyVal", method = {RequestMethod.POST,RequestMethod.GET})
    public ResultData delConfigKeyVal(@RequestParam("id")String id) {
        ConfigKeyVal configKeyVal = ConfigKeyVal.dao.findById(id);
        if (configKeyVal == null) {
//            renderJson(new ResultData().setMsg("查无数据，删除失败").setOk(false));
            return ResultData.buildFailure("查无数据，删除失败");
        }
        configKeyVal.delete();
        return ResultData.buildSuccess();
    }
    @RequestMapping(value = "/get", method = {RequestMethod.POST,RequestMethod.GET})
    public ResultData get(@RequestParam("id")String id) {
//        renderJson(new ResultData().setData(Config.dao.findById(id)));
        return ResultData.buildSuccess(Config.dao.findById(id));
    }

    @RequestMapping(value = "/saveUpdate", method = {RequestMethod.POST,RequestMethod.GET})
    public ResultData saveUpdate(@RequestParam("code")String code,@RequestParam("configName")String configName,@RequestParam("inputType")String inputType,@RequestParam("projectId")Integer projectId,@RequestParam( value = "configId",required = false)Integer configId) {
//        if (StrKit.isBlank(code)) {
//            renderJson(new ResultData().setOk(false).setMsg("code不能为空"));
//            return;
//        }
//        if (StrKit.isBlank(getPara("configName"))) {
//            renderJson(new ResultData().setOk(false).setMsg("配置名称不能为空"));
//            return;
//        }
//        if (StrKit.isBlank(getPara("inputType"))) {
//            renderJson(new ResultData().setOk(false).setMsg("类型不能为空"));
//            return;
//        }
//        if (StrKit.isBlank(getPara("projectId"))) {
//            renderJson(new ResultData().setOk(false).setMsg("项目不能为空"));
//            return;
//        }

        Config config = new Config();
        config.setCode(code);
        config.setConfigName(configName);
        config.setInputType(inputType);
        config.setProjectId(projectId);
        // 判断唯一性
        Config configTmp = Config.dao.findFirst("select * from config where project_id=? and `code`=?",
                config.getProjectId(), config.getCode());
        if (configTmp != null) {
            if (configId == null) {
//                renderJson(new ResultData().setOk(false).setMsg("code重复了"));
                return ResultData.buildFailure("code重复");
            } else if (configTmp.getConfigId() != configId) {
//                renderJson(new ResultData().setOk(false).setMsg("code重复了"));
                return ResultData.buildFailure("code重复");
            }
        }
        if (configId == null) {
            config.save();
        } else {
            config.setConfigId(configId);
            config.update();
        }

        return ResultData.buildSuccess();
    }
    @RequestMapping(value = "/configKeyValSaveUpdate", method = {RequestMethod.POST,RequestMethod.GET})
    public ResultData configKeyValSaveUpdate(@RequestParam(value = "id",required = false)Integer id,@RequestParam("name")String name,
                                             @RequestParam("val")String val,
                                             @RequestParam("configId")Integer configId,
                                             @RequestParam(value = "listImportPkg",required = false)String listImportPkg,
                                             @RequestParam(value = "val1",required = false)String val1,
                                             @RequestParam(value = "val2",required = false)String val2) {
//        if (StrKit.isBlank(getPara("name"))) {
//            renderJson(new ResultData().setOk(false).setMsg("name不能为空"));
//            return;
//        }
//        if (StrKit.isBlank(getPara("val"))) {
//            renderJson(new ResultData().setOk(false).setMsg("val不能为空"));
//            return;
//        }
//        if (StrKit.isBlank(getPara("configId"))) {
//            renderJson(new ResultData().setOk(false).setMsg("configId不能为空"));
//            return;
//        }

        ConfigKeyVal configKeyVal = new ConfigKeyVal();
        configKeyVal.setConfigId(configId);
        configKeyVal.setName(name);
        configKeyVal.setVal(val);
        configKeyVal.setVal1(val1);
        configKeyVal.setVal2(val2);
        configKeyVal.setListImportPkg(listImportPkg);
        // 判断唯一性
        if (id == null) {
            configKeyVal.save();
        } else {
            configKeyVal.setId(id);
            configKeyVal.update();
        }
        return ResultData.buildSuccess();
    }
}