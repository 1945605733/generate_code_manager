package com.code.generate.config;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.code.generate.model._MappingKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ActiveRecordInit implements InitializingBean {
//    @Value("${dbUrl}")
    @NacosValue(value = "${dbUrl}", autoRefreshed = true)
    String dbUrl;

//    @Value("${dbUserName}")
    @NacosValue(value = "${dbUserName}", autoRefreshed = true)
    String dbUserName;

//    @Value("${dbPassword}")
    @NacosValue(value = "${dbPassword}", autoRefreshed = true)
    String dbPassword;

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("activeRecordInit 开始");
        log.info("activeRecordInit param  dbUrl:{} , dbUserName:{} , dbPassword:{} ",dbUrl, dbUserName, dbPassword);
        DruidPlugin dp = new DruidPlugin(dbUrl, dbUserName, dbPassword);
        ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
        arp.setShowSql(true);
        _MappingKit.mapping(arp);
        dp.start();
        arp.start();
        log.info("activeRecordInit 结束");
    }
}
