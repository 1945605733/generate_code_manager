package com.code.generate.vo;

public class ResultData {
	public ResultData() {
	}
	public ResultData(boolean ok,String msg) {
		this.ok=ok;
		this.msg=msg;
	}
	
	private Boolean ok = true;


	public Boolean getOk() {
		return ok;
	}
	public ResultData setOk(Boolean ok) {
		this.ok = ok;
		return this;
	}
	public ResultData setOk(boolean ok) {
		this.ok = ok;
		return this;
	}
	
	private String msg = "请求成功";

	public String getMsg() {
		return msg;
	}

	public ResultData setMsg(String msg) {
		this.msg = msg;
		return this;
	}
	private Object data;

	public Object getData() {
		return data;
	}
	public ResultData setData(Object data) {
		this.data = data;
		return this;
	}
	public static ResultData buildSuccess(Object data){
		return new ResultData().setData(data);
	}
	public static ResultData buildSuccess(){
		return new ResultData();
	}
	public static ResultData buildFailure(){
		return new ResultData().setOk(false).setMsg("处理失败");
	}
	public static ResultData buildFailure(String msg){
		return new ResultData().setMsg(msg);
	}
	
}
